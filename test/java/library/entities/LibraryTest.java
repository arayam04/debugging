package library.entities;

import library.entities.helpers.IPatronHelper;
import org.junit.jupiter.api.Test;


import java.awt.desktop.SystemSleepEvent;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
class LibraryTest {

    Library library;
    @Test
    void calculateOverDueFineforEachDay() {
        // arrange
        double expected = 1.00;
        Book book = new Book("Java", "Programming","111", 1);
        Patron patron = new Patron ("rayamajhi", "amit", "amit@gmail.com", 123456,1);
        Loan loan = new Loan(book, patron,null, ILoan.LoanState.OVER_DUE,0);
        assertTrue(loan.isOverDue());

        Date dueDate = new Date("10/19/2020");
        Date currentDate = Calendar.getInstance().getDate();
        long daysOverDue = Calendar.getInstance().getDaysDifference(dueDate);
        System.out.println("Due date " + dueDate + " current date =" + currentDate);
        // act
        double actual = daysOverDue * library.FINE_PER_DAY;

        //System.out.println("Expected = " + expected);
        //System.out.println("Actual = " + actual);

        // assert
        assertEquals(expected,actual);
    }
    @Test
    void calculateOverDueFine() {
        // arrange
        double expected = 10.00;
        Book book = new Book("Java", "Programming","111", 1);
        Patron patron = new Patron ("rayamajhi", "amit", "amit@gmail.com", 123456,1);
        Loan loan = new Loan(book, patron,null, ILoan.LoanState.OVER_DUE,0);
        assertTrue(loan.isOverDue());

        Date dueDate = new Date("10/10/2020");
        Date currentDate = Calendar.getInstance().getDate();
        long daysOverDue = Calendar.getInstance().getDaysDifference(dueDate);
        System.out.println("Due date " + dueDate + " current date =" + currentDate);
        // act
        double actual = daysOverDue * library.FINE_PER_DAY;

        //System.out.println("Expected = " + expected);
        //System.out.println("Actual = " + actual);

        // assert
        assertEquals(expected,actual);
    }
}